/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt1.pkg2.ex2;

import java.io.IOException;
import java.util.Scanner;
/**
 *
 * @author Ainhoa Lopez Barceló
 * @author Pablo Rodriguez 
 */
public class Ex2Adn {

    private String[] menu = {
        "1-Dar la vuelta",
        "2-Encontrar la base mas repetida",
        "3-Encontrar la base menos repetida",
        "4-Recuento de bases",
        "5-Salir"
    };
    
    public static void main(String[] args) throws IOException{
        System.out.println("Hello World");
        /*ej1 ex = new ej1();
        ex.menu();*/
        
    }
    
    private void menu() throws IOException{
        int option = 5;
        do{
            option = showMenu();
            
            switch(option){
                case 1: System.out.println("1-Dar la vuelta");
                        break;
                case 2: System.out.println("2-Encontrar la base mas repetida");
                        break;
                case 3: System.out.println("3-Encontrar la base menos repetida");
                        break;
                case 4: System.out.println("4-Recuento de bases");
                        break;
                case 5: System.out.println("Menu finalizado!!");
                        break;
                default:
                    System.out.println("Opcion incorrecta. Solo puede ser: 1, 2, 3 o 4");
                        break;
            }
        }while(option!=5);
    }

    private int showMenu() {
        int option = -1;
        int i;
        
        System.out.println("Menu:");
        
        for (i = 0; i < menu.length; i++) {
            System.out.format("%d %s \n", i+1, menu[i]);
        }

        Scanner sc = new Scanner(System.in);
        
        System.out.print("Choose a menu option: ");
        option = sc.nextInt();
        
        return option;
    }
}
